/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author adj006
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		String s = Float.toString(super.getValue());
		return "" + s + " C";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		Temperature c = new Celsius( super.getValue());
		return c;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float e = ( (9 * (super.getValue() ) /5) + 32 ) ;
		Temperature c = new Fahrenheit( e );
		//f.value = (f.getValue() - 32)*(5/9);
		return c;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		Temperature c = new Celsius( super.getValue() + 273 );
	
		return c;	
		}
}
