/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author Ricky
 *
 */
public class Kelvin extends Temperature {

	public Kelvin(float t) 
	{ 
	 super(t); 
	} 
	 
	public String toString() 
	{ 
		String s = Float.toString(super.getValue());
		return "" + s + " K";
	} 

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toCelsius()
	 */
	@Override
	public Temperature toCelsius() {
		Temperature k = new Celsius( super.getValue() - 273);
		return k;
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toFahrenheit()
	 */
	@Override
	public Temperature toFahrenheit() {
		float e = ( ((9 * (super.getValue() - 273 )) / 5) + 32 ) ;
		Temperature c = new Fahrenheit( e );
		//f.value = (f.getValue() - 32)*(5/9);
		return c;
	}

	@Override
	public Temperature toKelvin() {
		Temperature c = new Kelvin( super.getValue());
		return c;
	}

}
