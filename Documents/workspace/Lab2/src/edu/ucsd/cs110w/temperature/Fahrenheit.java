/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author adj006
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		String s = Float.toString( super.getValue() );

		return "" + s + " F";
		//return "";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float e = (5 * (super.getValue() - 32) / 9);
		Temperature f = new Fahrenheit( e );
		//f.value = (f.getValue() - 32)*(5/9);
		return f;
		//return null;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		//return ((9/5)*this.getValue() + 32);
		Temperature f = new Fahrenheit( super.getValue());
		return f;
	}
	@Override
	public Temperature toKelvin() {
		float e = ( 5 * ( super.getValue() - 32 ) / 9 ) + 273; 
		Temperature k = new Fahrenheit( e );
		return k;
	}
}
